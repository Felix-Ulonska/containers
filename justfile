tag := 'latest'

build: 
	#!/usr/bin/env bash
	set -euxo pipefail	
	for d in */ ; do
		docker build ${d::-1} -t "$CI_REGISTRY/${d::-1}:{{tag}}"
	done

push: build
	#!/usr/bin/env bash
	set -euxo pipefail	
	for d in */ ; do
		docker push "$CI_REGISTRY/${d::-1}:{{tag}}"
	done

